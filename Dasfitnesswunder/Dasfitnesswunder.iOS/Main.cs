﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace Dasfitnesswunder.iOS
{
    public class Application
    {
        // This is the main entry point of the application.
        static void Main(string[] args)
        {
            //System.Threading.Tasks.Task.Run(() =>
            //{
            //    var service = new ThryveHandlerService();
            //    service.Connect("", "");
            //    //Console.WriteLine("iOSente: ");
            //    //service.Connector.GetAccessTokenOnSuccess((x) => Console.WriteLine(x), (x) => Console.WriteLine("error getting token"));
            //});

            // if you want to use a different Application Delegate class from "AppDelegate"
            // you can specify it here.
            try
            {
                UIApplication.Main(args, null, "AppDelegate");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
