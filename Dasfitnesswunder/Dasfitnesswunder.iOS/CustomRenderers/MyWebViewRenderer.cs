﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Dasfitnesswunder;
using Dasfitnesswunder.iOS.CustomRenderers;
using UIKit;
using WebKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(AutoWebView), typeof(MyWebViewRenderer))]

namespace Dasfitnesswunder.iOS.CustomRenderers
{
    public class MyWebViewRenderer : ViewRenderer<AutoWebView, WkWebViewRenderer>
    {
        WKWebView _wkWebView;

        protected override void OnElementChanged(ElementChangedEventArgs<AutoWebView> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
            {
                WKWebViewConfiguration config = new WKWebViewConfiguration();

                _wkWebView = new WKWebView(Frame, config);
                _wkWebView.NavigationDelegate = new ExtendedUIWebViewDelegate(this);
            }
            if (e.NewElement != null)
            {

                HtmlWebViewSource source = (Xamarin.Forms.HtmlWebViewSource)Element.Source;
                string headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'></header>";
                string html = headerString + source.Html;
                Console.WriteLine("Height" + Element);
                _wkWebView.LoadHtmlString(html, baseUrl: null);
                _wkWebView.ScrollView.ScrollEnabled = false;
                _wkWebView.SizeToFit();
            }
        }
    }

    public class ExtendedUIWebViewDelegate : WKNavigationDelegate
    {
        MyWebViewRenderer webViewRenderer;

        public ExtendedUIWebViewDelegate(MyWebViewRenderer _webViewRenderer = null)
        {
            webViewRenderer = _webViewRenderer ?? new MyWebViewRenderer();
        }

        async public override void DidFinishNavigation(WKWebView webView, WKNavigation navigation)
        {
            var wv = webViewRenderer.Element as AutoWebView;
            if (wv != null)
            {
                await System.Threading.Tasks.Task.Delay(100); // wait here till content is rendered
                wv.HeightRequest = (double)webView.ScrollView.ContentSize.Height;
                (wv.Parent.Parent.Parent as ViewCell).ForceUpdateSize();
            }

            //var wv = webViewRenderer.Element as AutoWebView;
            //if (wv.HeightRequest < 0)
            //{
            //    wv.HeightRequest = (double)webView.ScrollView.ContentSize.Height;
            //    (wv.Parent.Parent.Parent as ViewCell).ForceUpdateSize();
            //}
        }
    }
}