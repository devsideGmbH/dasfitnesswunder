﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Dasfitnesswunder.CustomControls;
using Dasfitnesswunder.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace Dasfitnesswunder.iOS.CustomRenderers
{
    public class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Editor> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Layer.CornerRadius = 4;
                Control.Layer.BorderColor = Color.LightGray.ToCGColor();
                Control.Layer.BorderWidth = 1;
            }
        }
    }
}