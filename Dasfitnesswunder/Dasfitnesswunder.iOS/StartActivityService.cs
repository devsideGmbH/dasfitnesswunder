﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Dasfitnesswunder.iOS;
using Dasfitnesswunder.Pages;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(StartActivityService))]
namespace Dasfitnesswunder.iOS
{
    class StartActivityService : IStartActivityService
    {
        public CompanyDetailsPage_Register Caller { get; set ; }

        public void StartThryveLoginActivity(int companyId, int userId, int thryveSourceId)
        {
            var window = UIApplication.SharedApplication.Delegate.GetWindow();
            var rvc = window.RootViewController;
            var vc = new ThryveSdkActivity
            {
                companyId = companyId,
                userId = userId,
                thryveSourceId = thryveSourceId
            };
            vc.Caller = Caller;

            vc.Service.Connect("", "");

            vc.Service.Connector.GetAccessTokenOnSuccess(
                    (accessToken) =>
                    {
                        vc.AccessToken = accessToken;
                        Console.WriteLine("AccessToken: " + accessToken);
                        rvc.PresentViewController(vc, true, () => { });
                    },
                    (error) =>
                    {
                        Console.WriteLine("AccessToken: " + error);
                    }
                );
        }
    }
}