﻿using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using WebKit;
//using ThryveCoreSDK;
using Xamarin.Forms;
using Dasfitnesswunder.Data;
using Dasfitnesswunder.Services;
using Dasfitnesswunder.Pages;

namespace Dasfitnesswunder.iOS
{
    [Register("UniversalView")]
    public class UniversalView : UIView
    {
        public UniversalView()
        {
            Initialize();
        }

        public UniversalView(RectangleF bounds) : base(bounds)
        {
            Initialize();
        }

        void Initialize()
        {
            BackgroundColor = UIColor.Red;
        }
    }

    public class WebViewDelegate : WKUIDelegate
    {

    }


    public class WebViewNavigationDelegate : WKNavigationDelegate
    {
        public EventHandler ValueChanged;

        public override void DidFinishNavigation(WKWebView webView, WKNavigation navigation)
        {
            ValueChanged(null, null);
        }
    }

    [Register("ThryveSdkActivity")]
    public class ThryveSdkActivity : UIViewController
    {
        public int companyId;
        public int userId;
        public int thryveSourceId;

        private WKWebView webView;
        private WebViewDelegate webViewDelegate;
        public WebViewNavigationDelegate WebViewNavigationDelegate;
        public ThryveHandlerService Service { get;  set; }
        public NSString AccessToken { get;  set; }
        public CompanyDetailsPage_Register Caller { get; internal set; }

        public ThryveSdkActivity()
        {
            webViewDelegate = new WebViewDelegate();
            WebViewNavigationDelegate = new WebViewNavigationDelegate();
            Service = new ThryveHandlerService();
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            if (Caller != null)
            {
                Caller.CallOnAppering();
            }
        }
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public void ShowAlertDialog(bool connected)
        {
            string message = "AccessToken: " + AccessToken + "\nStatus: ";

            switch (connected)
            {
                case true:
                    message += "Connected";
                    break;
                case false:
                    message += "Revoked or was not connected beforehand";
                    break;
            }

            UIAlertController alert = UIAlertController.Create("Connection status", message, UIAlertControllerStyle.Alert);
            alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, (action) =>
            {
                NavigationController.PopViewController(true);
            }));

            PresentViewController(alert, true, null);
        }

        public override void ViewDidLoad()
        {
            View = new UniversalView();

            webView = new WKWebView(View.Frame, new WKWebViewConfiguration());
            webView.UIDelegate = webViewDelegate;
            webView.NavigationDelegate = WebViewNavigationDelegate;

            WebViewNavigationDelegate.ValueChanged += (s, e) =>
            {
                try
                {
                    Service.Connector.ResponseHandleDataSourceDirectConnectionOnResult((Action<NSUrl, bool>)((url, connected) =>
                    {
                        if (connected)
                        {
                            var generalService = new GeneralService(new RestService((string)Constants.RESTAPIKEY));

                            if (generalService != null)
                            {
                                var thryveRes = generalService.SetThryveToken(companyId, userId, AccessToken);
                                Console.WriteLine("SetThryveToken: " + thryveRes);
                            }
                            Dasfitnesswunder.Pages.CompanyDetailsPage_Register.RegistrationCompleteSuccessfully = true;
                            base.DismissViewController(true, () => { });
                        }
                    }),
                    (error) =>
                    {
                        Console.WriteLine(error.LocalizedDescription);
                    });
                }
                catch (Exception err)
                {
                    Console.WriteLine("ResponseHandleDataSourceDirectConnectionOnResult: " + err);
                }
            };


            View.AddSubview(webView);

            webView.TranslatesAutoresizingMaskIntoConstraints = false;
            webView.LeadingAnchor.ConstraintEqualTo(View.LeadingAnchor, constant: 0).Active = true;
            webView.TrailingAnchor.ConstraintEqualTo(View.TrailingAnchor, constant: 0).Active = true;
            webView.TopAnchor.ConstraintEqualTo(View.TopAnchor, constant: 0).Active = true;
            webView.BottomAnchor.ConstraintEqualTo(View.BottomAnchor, constant: 0).Active = true;

            Service.Connector.RequestHandleDataSourceDirectConnectionWithIsConnection(true, false, new ThryveCoreSDK.Tracker("", thryveSourceId), webView, (error) =>
            {
                Console.WriteLine("HandleDataSourceConnectionWithLoadOn: " + error);
            });

            base.ViewDidLoad();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }
    }
}