﻿using System;
using System.Drawing;

using CoreFoundation;
using UIKit;
using Foundation;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace Dasfitnesswunder.iOS
{
    [Register("MainUniversalView")]
    public class MainUniversalView : UIView
    {
        public MainUniversalView()
        {
            Initialize();
        }

        public MainUniversalView(RectangleF bounds) : base(bounds)
        {
            Initialize();
        }

        void Initialize()
        {
            BackgroundColor = UIColor.Red;
        }
    }

    [Register("MainViewController")]
    public class MainViewController : UIViewController
    {
        public MainViewController(IntPtr handle) : base(handle)
        {
            //System.Threading.Tasks.Task.Run(() =>
            //{
            //    //var sas = DependencyService.Get<IStartActivityService>();

            //    var service = new ThryveHandlerService();
            //    service.Connect("", "");

            //    //Console.WriteLine("iOSente: ");
            //    //service.Connector.GetAccessTokenOnSuccess((x) => Console.WriteLine(x), (x) => Console.WriteLine("error getting token"));
            //});
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            View = new MainUniversalView();

            base.ViewDidLoad();

            // Perform any additional setup after loading the view
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }
    }
}