//
//  CoreSDK.h
//  CoreSDK
//
//  Created by Florian on 14.10.19.
//  Copyright © 2019 Undgesund. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>

//! Project version number for CoreSDK.
FOUNDATION_EXPORT double CoreSDKVersionNumber;

//! Project version string for CoreSDK.
FOUNDATION_EXPORT const unsigned char CoreSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreSDK/PublicHeader.h>


