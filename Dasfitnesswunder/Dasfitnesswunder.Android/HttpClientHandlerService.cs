﻿using System.Net.Http;
using System.Net.Security;
using Dasfitnesswunder.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(HttpClientHandlerService))]

namespace Dasfitnesswunder.Droid
{
    public class HttpClientHandlerService : IHttpClientHandlerService
    {
        public HttpClientHandler GetInsecureHandler()
        {
            var httpHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
                {
                    if (cert.Issuer.Equals("CN=localhost")) return true;

                    return errors == SslPolicyErrors.None;
                }
            };

            return httpHandler;
        }
    }
}