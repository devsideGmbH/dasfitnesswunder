﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace Dasfitnesswunder.Droid
{
    [Activity(Label = "SplashActivity", Theme = "@style/MainTheme", MainLauncher = false, NoHistory = true)]
    public class SplashActivity : FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            DependencyService.Register<IStartActivityService, StartActivityService>();

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());

            //var a = Application.Context;
            

            //StartActivity(new Intent(Application.Context, typeof(MainActivity)));
            //// Create your application here
        }
    }
}