﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Dasfitnesswunder.Droid;
using Dasfitnesswunder.Pages;
using Xamarin.Forms;

[assembly: Dependency(typeof(StartActivityService))]
namespace Dasfitnesswunder.Droid
{
    class StartActivityService : IStartActivityService
    {
        CompanyDetailsPage_Register IStartActivityService.Caller { get; set; }

        public void StartThryveLoginActivity(int companyId, int userId, int thryveSourceId)
        {
            var intent = new Intent(Forms.Context, typeof(ThryveSdkActivity));

            intent.PutExtra("companyId", companyId);
            intent.PutExtra("userId", userId);
            intent.PutExtra("thryveSourceId", thryveSourceId);

            Forms.Context.StartActivity(intent);
        }
    }
}