﻿using Android.Content;
using Com.Thryve.Connector.Sdk;
using Dasfitnesswunder.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(ThryveHandlerService))]

namespace Dasfitnesswunder.Droid
{
    public class ThryveHandlerService : IThryveHandlerService
    {

        public CoreConnector Connector { get; private set; }

        public void Connect(Context context, string appId, string appSecret) //, string partnerUserId)
        {
            Connector = new CoreConnector(context, appId, appSecret); //, partnerUserId);
        }

        public void Connect(string partnerUserId, string appId)
        {
            throw new System.NotImplementedException();
        }
    }
}