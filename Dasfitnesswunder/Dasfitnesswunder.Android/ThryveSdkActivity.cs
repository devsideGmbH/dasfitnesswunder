﻿using System;

using Android.App;
using Android.OS;
using Dasfitnesswunder.Data;
using Dasfitnesswunder.Pages;
using Dasfitnesswunder.Services;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace Dasfitnesswunder.Droid
{
    public class ThryveConnectorCallbacks : Com.Thryve.Connector.Sdk.ConnectorCallback
    {
        public ThryveSdkActivity ThryveSdkActivity { get; private set; }
        public ThryveConnectorCallbacks(ThryveSdkActivity thryveSdkActivity)
        {
            ThryveSdkActivity = thryveSdkActivity;
        }

        public override void OnError(string accessToken, int sourceApi, string message)
        {
            Console.WriteLine($"token: { accessToken } sourceApi: {sourceApi} message: {message} ");
        }

        public override async void OnResult(string accessToken, int sourceApi, bool success, string message)
        {
            Console.WriteLine($"token: { accessToken } sourceApi: {sourceApi} success: {success} message: {message} ");

            var generalService = new GeneralService(new RestService(Constants.RESTAPIKEY));

            if (generalService != null)
            {
                var thryveRes = await generalService.SetThryveToken(ThryveSdkActivity.companyId, ThryveSdkActivity.userId, accessToken);
                Console.WriteLine("SetThryveToken: " + thryveRes);
            }

            if (ThryveSdkActivity != null)
            {
                //Xamarin.Forms.DependencyService.Get<INavigation>().PushAsync(new Success());
                //NavigableElement.NavigationProperty.
                Dasfitnesswunder.Pages.CompanyDetailsPage_Register.RegistrationCompleteSuccessfully = true;
                ThryveSdkActivity.Finish();
            }
        }
    }

    [Activity(Label = "ThryveSdkActivity", Icon = "@mipmap/icon", Theme = "@style/MainTheme")]
    public class ThryveSdkActivity : Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public Android.Webkit.WebView WebView { get; set; }
        public ThryveHandlerService Service { get; set; }
        public ThryveConnectorCallbacks ThryveConnectorCallbacks { get; set; }
        public int companyId = -1;
        public int userId = -1;
        public int thryveSourceId = -1;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            companyId = Intent.GetIntExtra("companyId", -1);
            userId = Intent.GetIntExtra("userId", -1);
            thryveSourceId = Intent.GetIntExtra("thryveSourceId", -1);

            if (WebView == null)
            {
                WebView = new Android.Webkit.WebView(this);
            }
            SetContentView(WebView);

            if (Service == null)
            {
                Service = new ThryveHandlerService();
                ThryveConnectorCallbacks = new ThryveConnectorCallbacks(this);

                if (thryveSourceId != -1)
                {
                    System.Threading.Tasks.Task.Run(() =>
                    {
                        Service.Connect(this, Constants.appId, Constants.appSecret); //, "fuenfzig670");

                        Console.WriteLine("AccessToken: " + Service.Connector.AccessToken);
                        Service.Connector.HandleDataSourceDirectConnection(true, thryveSourceId, false, WebView, ThryveConnectorCallbacks);
                    });
                }
            }
        }

        public override void OnBackPressed()
        {
            //base.OnBackPressed();
            if (WebView.CanGoBack())
            {
                WebView.GoBack();
            }
            else
            {
                Finish();
            }

        }
    }
}