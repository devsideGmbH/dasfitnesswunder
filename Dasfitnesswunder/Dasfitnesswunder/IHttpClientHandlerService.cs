﻿using System.Net.Http;

namespace Dasfitnesswunder
{
    public interface IHttpClientHandlerService
    {
        HttpClientHandler GetInsecureHandler();
    }
}