﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dasfitnesswunder.Helpers
{
    public static class HtmlHelper
    {
        public static string AddiOSStylingHeader(string webStr)
        {
            var htmlStartWithStyle = @"
            <html>
                <head> 
                    <style> 
                        h1 { font-size: 5rem; }     
                        p { font-size: 2rem; }
                        li { font-size: 2rem; }
                        body { margin: 2rem; }
                    </style>
                </head>
                <body>
            ";

            var htmlEnd = @"
                </body> 
            </html>
            ";

            //return webStr.Replace("<head>", htmlStartWithStyle);
            return $"{htmlStartWithStyle}{webStr}{htmlEnd}";
        }

        public static string AddAndroidStylingHeader(string webStr)
        {

            var htmlStartWithStyle = @"
            <html>
                <head> 
                    <style> 
                        body { margin: 1rem; }
                        
                    </style>
                </head>
                <body>
            ";

            var htmlEnd = @"
                </body> 
            </html>
            ";

            //return webStr.Replace("<head>", htmlStartWithStyle);
            return $"{htmlStartWithStyle}{webStr}{htmlEnd}";
        }

        public static string MakeHeader(string text)
        {
            return $"<h1>{text}</h1>";
        }

        public static string GetHeaderImage()
        {
            return $"<img src={Constants.HeaderImagePath} style='width: 100%; height:auto;'>";
        }

    }
}
