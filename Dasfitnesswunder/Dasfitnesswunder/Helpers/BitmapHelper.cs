﻿using Dasfitnesswunder.Model;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Dasfitnesswunder.Helpers
{
    public static class BitmapHelper
    {

        public static void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            CustomSKCanvasView customSender = (CustomSKCanvasView)sender;
            SKImageInfo info = args.Info;
            SKSurface surface = args.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Clear();
            
            using (SKPaint paint = new SKPaint())
            {
                paint.ColorFilter =
                    SKColorFilter.CreateColorMatrix(new float[]
                    {
                        0.21f, 0.72f, 0.07f, 0, 0,
                        0.21f, 0.72f, 0.07f, 0, 0,
                        0.21f, 0.72f, 0.07f, 0, 0,
                        0,     0,     0,     1, 0
                    });

                if (customSender.Source != null) canvas.DrawBitmap(
                        SKBitmap.Decode(new MemoryStream(customSender.Source)),
                        info.Rect,
                        BitmapStretch.Uniform,
                        horizontal: customSender.Start ? BitmapAlignment.Start : BitmapAlignment.End,
                        paint: customSender.GrayScale ? paint : null
                    );
            }
        }

        public static bool IsChallengeExpired(Company firstCompany)
        {
            bool expired = true;
            DateTime now = DateTime.Now;

            foreach (var challenge in firstCompany.Challenges)
            {
                if (DateTime.Compare(now, challenge.Finish) < 0) expired = false;
            }

            return expired;
        }
    }
}
