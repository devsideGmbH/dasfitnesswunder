using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: ExportFont("Sriracha-Regular.ttf", Alias = "Sriracha")]
[assembly: ExportFont("Oswald-Regular.ttf", Alias = "Oswald")]
