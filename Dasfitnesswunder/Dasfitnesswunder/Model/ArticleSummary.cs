﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dasfitnesswunder.Model
{
    public class ArticleSummary
    {
        public int id { get; set; }
        public string headline { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }
}
