﻿namespace Dasfitnesswunder.Model
{
    public class UserRanking
    {
        public int Ranking { get; set; }
        public int Id { get; set; }
        public string Nickname { get; set; }
        public int Gender { get; set; }

        public string Steps { get; set; }

        public bool ShowGender { get; set; }
        public string GenderString
        {
            get
            {
                if (!ShowGender)
                {
                    return "";
                }
                return Gender == 0 ? "♂" : "♀";
            }
        }
    }
}