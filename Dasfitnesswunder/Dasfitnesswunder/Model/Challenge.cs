﻿using System;

namespace Dasfitnesswunder.Model
{
    public class Challenge
    {
        public string ChallengeName { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public DateTime EntryStart { get; set; }
        public int DaysVisible { get; set; }
    }
}