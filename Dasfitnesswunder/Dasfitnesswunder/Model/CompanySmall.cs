﻿using System;

namespace Dasfitnesswunder.Model
{
    public class CompanySmall
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public bool Show { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}