﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dasfitnesswunder.Model
{
    public class DropDown
    {
        public int id { get; set; }
        public string dropdowntitle { get; set; }
        public List<DropDownValue> dropdownvalues { get; set; }
    }
}
