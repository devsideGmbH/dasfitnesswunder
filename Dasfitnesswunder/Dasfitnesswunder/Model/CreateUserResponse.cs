﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dasfitnesswunder.Model
{
    public class CreateUserResponse
    {
        public int Id { get; set; } = -1;
        public bool FirstnameSet { get; set; } = true;
        public bool LastnameSet { get; set; } = true;
        public bool EmailSet { get; set; } = true;
        public bool NicknameSet { get; set; } = true;
        public bool Agb_confirmSet { get; set; } = true;
        public bool Polar_reg_confirmSet { get; set; } = true;
        public bool GenderSet { get; set; } = true;
        public bool DayofbirthSet { get; set; } = true;
        public bool BaseSet { get; set; } = true;
        public bool Userdropdowns_dropdownvalueSet { get; set; } = true;
        public List<string> Messages { get; set; } = new List<string>();

    }
}
