﻿using System;

namespace Dasfitnesswunder.Model
{
    public class Partner
    {
        public string PartnerName { get; set; }
        public string Logo { get; set; }
        public string Link { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}