﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dasfitnesswunder.Model
{
    public class CreateUserRequest
    {
        public string gender { get; set; }

        //public Dictionary<int, UserDropdownAttribute> userDropdownsAttributes { get; set; }

        public Dictionary<int, Dictionary<string, string>> userdropdowns_attributes = new Dictionary<int, Dictionary<string, string>>();

        public string dayofbirth { get; set; }
        public string nickname { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string email { get; set; }
        public bool polar_reg_confirm { get; set; }
        public bool agb_confirm { get; set; }
        public string api_source_id { get; set; }
    }
}
