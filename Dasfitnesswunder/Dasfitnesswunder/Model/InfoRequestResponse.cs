﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dasfitnesswunder.Model
{
    public class InfoRequestResponse
    {
        public bool Success { get; set; } = false;
        public bool FirstnameSet { get; set; } = true;
        public bool LastnameSet { get; set; } = true;
        public bool EmailSet { get; set; } = true;
        public bool MessageSet { get; set; } = true;
        public List<string> Messages { get; set; } = new List<string>();
    }
}
