﻿using System;
using System.Collections.Generic;

namespace Dasfitnesswunder.Model
{
    public class Company
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Slogan { get; set; }
        public string Logo { get; set; }
        public string ImageBigTeaser { get; set; }
        public string ImageRegistration { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public List<Challenge> Challenges { get; set; } = new List<Challenge>();
        public List<Partner> Partners { get; set; } = new List<Partner>();

        public List<ThryveSource> MyProperty { get; set; }

        public Registration registration { get; set; }
        public byte[] Source { get; set; }
    }
}