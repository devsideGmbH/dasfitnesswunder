﻿using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dasfitnesswunder.Model
{
    public class CustomSKCanvasView: SKCanvasView
    {
        public byte[] Source { get; set; }

        public SkiaSharp.SKBitmap SourceBitmap { get; set; }

        public bool Start { get; set; }

        public bool GrayScale { get; set; }

    }
}
