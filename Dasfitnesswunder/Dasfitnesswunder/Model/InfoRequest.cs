﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dasfitnesswunder.Model
{
    public class InfoRequest
    {
        public string phone { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string company { get; set; }
        public string employees { get; set; }
        public string email { get; set; }
        public string message { get; set; }
    }
}
