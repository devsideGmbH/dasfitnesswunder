﻿using System;
using Dasfitnesswunder.Pages;
using Xamarin.Forms;

namespace Dasfitnesswunder
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            Startup.Initialize();

            MainPage = new NavigationPage(new MainPage());
        }

        public IServiceProvider ServiceProvider { get; set; }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}