﻿using Dasfitnesswunder.Helpers;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Dasfitnesswunder.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        #region footer
        protected int _companyId = 1;
        private async void OnImpressumClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Impressum, _companyId, Constants.Impressum), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnPrivacyClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Datenschutz, _companyId, Constants.Datenschutz), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnConditionsClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Teilnahmebedingungen, _companyId, Constants.Teilnahmebedingungen), true);
            ((View)sender).IsEnabled = true;

        }
        #endregion

        public AboutPage()
        {
            InitializeComponent();
            //var webStr = "<div class='row contentpage'><div class='col-sm-12'><h1>Was ist Dasfitnesswunder?</h1>  </div></div><div class='row contentpage'>  <div class='col-sm-12'><!-- error message container --><!-- /error message container end -->  <p>Dasfitnesswunder ist eine einzigartige Challenge, die Bewegung, Wettkampf und Spaß kombiniert und dabei Fitness und Leistungsfähigkeit fördert.&nbsp;</p><p>&nbsp;</p><p><strong>Die Ziele:</strong></p><ul><li>Bewegung statt Trägheit.<br><br></li><li>Ein begeisterndes Ziel statt Stillstand.<br><br></li><li>Eine Community, die Dich unterstützt – statt Einzelkampf.<br><br></li></ul><p>Schließ Dich uns an! Du bist herzlich willkommen in der Gemeinschaft der „Schrittmacher“!</p><p>&nbsp;</p></div></div><div class='row contentpage'>    <div class='col-sm-12'>    <h1>Wie funktioniert Dasfitnesswunder?</h1>    </div>  </div>  <div class='row contentpage'>    <div class='col-sm-12'>    <!-- error message container -->  <!-- /error message container end -->      <ul>  <li>Jeder/Jede Teilnehmer/-in erhält einen Polar Fitnesstracker – so wird seine sportliche Leistung messbar.</li>  </ul>  <ul>  <li>Wer die meisten Schritte macht, liegt vorne. Dazu werden auf einer individuellen Challenge-Seite automatisch Ranglisten aktualisiert.</li>  </ul>  <ul>  <li>Der „Schweinehund-Besieger“ am Handgelenk garantiert die gleichen Voraussetzungen für alle Teilnehmer(inen).</li>  </ul>  <ul>  <li>Die Challenge sorgt für mehr Bewegung und stärkt gleichzeitig die Gesundheit.</li>  </ul>  <p>&nbsp;</p>  <p><strong>Die Mission: 3 simple Regeln</strong></p>  <ul>  <li>Möglichst viele Schritte gehen</li>  </ul>  <ul>  <li>Jeden Tag</li>  </ul>  <ul>  <li>Synchronisieren und Deinen Kollegen zeigen was in Dir steckt</li>  </ul>  <p>&nbsp;</p>  <p>Hier noch ein paar Beispiele, bei denen man nicht schwitzen muss und ganz einfach sein Ziel erreichen kann. Die einfachen Dinge machen den alltäglichen Unterschied, so werden Schritte im „Vorbeigehen“ gesammelt:</p>  <ul>  <li>Eine Haltestelle vorher aussteigen<br><br></li>  <li>Einen hinteren Parkplatz nutzen oder einen Block weiter weg parken<br><br></li>  <li>Die Mittagspause mit einem Spaziergang kombinieren<br><br></li>  <li>Den Aufzug vergessen und die Treppe neu entdecken<br><br></li>  <li>Kleinere Strecken spazierend bewältigen<br><br></li>  <li>Aufstehen beim Telefonieren<br><br></li>  <li>Jeder&nbsp;Gang zur Kantine, zum Kopierer und zum Kollegen&nbsp;zählt Schritte</li>  </ul>  <p>&nbsp;</p>    </div>  </div>  <div class='row contentpage'>    <div class='col-sm-12'>    <h1>Warum Du mitmachen solltest?</h1>    </div>  </div>  <div class='row contentpage'>    <div class='col-sm-12'>    <!-- error message container -->  <!-- /error message container end -->      <p>Weil:</p>  <ul>  <li>mehr Bewegung im Alltag gut tut und eine Challenge Spaß macht.</li>  </ul>  <ul>  <li>Du ein Mensch bist, der sich Ziele setzt und diese auch erreichen will.</li>  </ul>  <ul>  <li>Du im Vorbeigehen mehr Kalorien verbrennst und ungeliebte Fettpolster abbaust.</li>  </ul>  <ul>  <li>Du Dich immer wieder aufraffst, auch wenn es Tage geben wird, an denen Du lieber „auf dem Sofa“ bleiben möchtest, als Schritte zu machen.</li>  </ul>  <ul>  <li>Du Dein Herz stärken willst und Dich vitaler und leistungsfähiger fühlen willst.</li>  </ul>  <ul>  <li>Du ein neues Lebensgefühl erlebst.</li>  </ul>  <p>&nbsp;</p>  <p>Bewegung im Alltag ist erst der Anfang für ein gesundes Lebensgefühl. Du wirst Dich wundern, wie schnell Du Deine Fitness verbessern kannst.</p>  <p>Ärzte und Wissenschaftler raten zu täglich 10.000 Schritten*. Wenn Du es beispielsweise schaffst, jeden Tag 10.000 Schritte zu gehen, dann bist Du nicht nur ziemlich gut, sondern auch in 100 Tagen Millionär – Schrittemillionär!&nbsp;</p>  <p>&nbsp;</p>  <p>&nbsp;</p>  <p>*Quelle: http://www.medizin-welt.info/aktuell/10000-Schritte-am-Tag-zum-Abnehmen-und-gesund-bleiben/202</p>    </div>  </div>";
            var webStr = "<img src='https://www.dasfitnesswunder.de/assets/default_header-d94a990c7eeb4ab8d7577fd593739a5c2b9ae656f7831163c791851dfa8c8910.jpg' style='width: 100%; height:auto;'> <div class='row contentpage'> <div class='col-sm-12'><h1>Was ist Dasfitnesswunder?</h1></div> </div> <div class='row contentpage'> <div class='col-sm-12'> <!-- error message container --><!-- /error message container end --> <p> Dasfitnesswunder ist eine einzigartige Challenge, die Bewegung, Wettkampf und Spaß kombiniert und dabei Fitness und Leistungsfähigkeit fördert.&nbsp; </p> <p>&nbsp;</p> <p><strong>Die Ziele:</strong></p> <ul> <li>Bewegung statt Trägheit.<br /><br /></li> <li>Ein begeisterndes Ziel statt Stillstand.<br /><br /></li> <li> Eine Community, die Dich unterstützt – statt Einzelkampf.<br /><br /> </li> </ul> <p> Schließ Dich uns an! Du bist herzlich willkommen in der Gemeinschaft der „Schrittmacher“! </p> <p>&nbsp;</p> </div> </div> <div class='row contentpage'> <div class='col-sm-12'><h1>Wie funktioniert Dasfitnesswunder?</h1></div> </div> <div class='row contentpage'> <div class='col-sm-12'> <!-- error message container --> <!-- /error message container end --> <ul> <li> Jeder/Jede Teilnehmer/-in erhält einen Polar Fitnesstracker – so wird seine sportliche Leistung messbar. </li> </ul> <ul> <li> Wer die meisten Schritte macht, liegt vorne. Dazu werden auf einer individuellen Challenge-Seite automatisch Ranglisten aktualisiert. </li> </ul> <ul> <li> Der „Schweinehund-Besieger“ am Handgelenk garantiert die gleichen Voraussetzungen für alle Teilnehmer(inen). </li> </ul> <ul> <li> Die Challenge sorgt für mehr Bewegung und stärkt gleichzeitig die Gesundheit. </li> </ul> <p>&nbsp;</p> <p><strong>Die Mission: 3 simple Regeln</strong></p> <ul> <li>Möglichst viele Schritte gehen</li> </ul> <ul> <li>Jeden Tag</li> </ul> <ul> <li>Synchronisieren und Deinen Kollegen zeigen was in Dir steckt</li> </ul> <p>&nbsp;</p> <p> Hier noch ein paar Beispiele, bei denen man nicht schwitzen muss und ganz einfach sein Ziel erreichen kann. Die einfachen Dinge machen den alltäglichen Unterschied, so werden Schritte im „Vorbeigehen“ gesammelt: </p> <ul> <li>Eine Haltestelle vorher aussteigen<br /><br /></li> <li> Einen hinteren Parkplatz nutzen oder einen Block weiter weg parken<br /><br /> </li> <li> Die Mittagspause mit einem Spaziergang kombinieren<br /><br /> </li> <li>Den Aufzug vergessen und die Treppe neu entdecken<br /><br /></li> <li>Kleinere Strecken spazierend bewältigen<br /><br /></li> <li>Aufstehen beim Telefonieren<br /><br /></li> <li> Jeder&nbsp;Gang zur Kantine, zum Kopierer und zum Kollegen&nbsp;zählt Schritte </li> </ul> <p>&nbsp;</p> </div> </div> <div class='row contentpage'> <div class='col-sm-12'><h1>Warum Du mitmachen solltest?</h1></div> </div> <div class='row contentpage'> <div class='col-sm-12'> <!-- error message container --> <!-- /error message container end --> <p>Weil:</p> <ul> <li> mehr Bewegung im Alltag gut tut und eine Challenge Spaß macht. </li> </ul> <ul> <li> Du ein Mensch bist, der sich Ziele setzt und diese auch erreichen will. </li> </ul> <ul> <li> Du im Vorbeigehen mehr Kalorien verbrennst und ungeliebte Fettpolster abbaust. </li> </ul> <ul> <li> Du Dich immer wieder aufraffst, auch wenn es Tage geben wird, an denen Du lieber „auf dem Sofa“ bleiben möchtest, als Schritte zu machen. </li> </ul> <ul> <li> Du Dein Herz stärken willst und Dich vitaler und leistungsfähiger fühlen willst. </li> </ul> <ul> <li>Du ein neues Lebensgefühl erlebst.</li> </ul> <p>&nbsp;</p> <p> Bewegung im Alltag ist erst der Anfang für ein gesundes Lebensgefühl. Du wirst Dich wundern, wie schnell Du Deine Fitness verbessern kannst. </p> <p> Ärzte und Wissenschaftler raten zu täglich 10.000 Schritten*. Wenn Du es beispielsweise schaffst, jeden Tag 10.000 Schritte zu gehen, dann bist Du nicht nur ziemlich gut, sondern auch in 100 Tagen Millionär – Schrittemillionär!&nbsp; </p> <p>&nbsp;</p> <p>&nbsp;</p> <p> *Quelle: http://www.medizin-welt.info/aktuell/10000-Schritte-am-Tag-zum-Abnehmen-und-gesund-bleiben/202 </p> </div> </div>";

            if (Device.RuntimePlatform == Device.iOS)
            {
                webStr = HtmlHelper.AddiOSStylingHeader(webStr);
            }
            else
            {
                webStr = HtmlHelper.AddAndroidStylingHeader(webStr);
            }

            AboutWebView.Source = new HtmlWebViewSource { Html = webStr };            
        }


    }
}