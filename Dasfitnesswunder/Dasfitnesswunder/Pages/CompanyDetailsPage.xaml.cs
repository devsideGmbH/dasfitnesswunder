﻿using Dasfitnesswunder.Data;
using Dasfitnesswunder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Markup;
using Xamarin.Forms.Xaml;

namespace Dasfitnesswunder.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompanyDetailsPage : ContentPage
    {
        private bool _registrationActive = true;
        private Company _companyData;
        private List<RankingCategory> _rankingCategories;
        private List<RankingCategory> _dynamicRankingCategories;
        private List<RankingCategory> _allRankingCategories;

        private List<List<UserRanking>> _userRankings = new List<List<UserRanking>>();
        private readonly ICompanyDataService _companyDataService;
        public List<UserRanking> ChallengeResults { get; set; }
        private static int LogoSize = 60;

        #region footer
        protected int _companyId = 1;
        private async void OnImpressumClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Impressum, _companyId, Constants.Impressum), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnPrivacyClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Datenschutz, _companyId, Constants.Datenschutz), true);
            ((View)sender).IsEnabled = true;
        }
        private async void OnConditionsClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Teilnahmebedingungen, _companyId, Constants.Teilnahmebedingungen), true);
            ((View)sender).IsEnabled = true;

        }
        #endregion

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
        }

        public CompanyDetailsPage(Company companyData)
        {
            InitializeComponent();
            _companyDataService = Startup.ServiceProvider.GetService<ICompanyDataService>();
            _companyData = companyData;
            _companyId = companyData.Id;
            BindingContext = this;
            CompanyLogo.Success += (sender, e) =>
            {
                var height = e.ImageInformation.OriginalHeight;
                var width = e.ImageInformation.OriginalWidth;

                var ratio = (double)LogoSize / (double)height;
                CompanyLogo.HeightRequest = LogoSize;
                CompanyLogo.WidthRequest = width * ratio;
            };
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            StartLoading();
            IsBusy = true;

            ForceHeights();

            var utcNow = DateTime.UtcNow;

            if (_companyData == null) return;

            //set Logo and Slogan
            if (_companyData.Logo != null)
            {
                CompanyLogo.Source = $"{Constants.BASEURL}{_companyData.Logo}";
                CompanyLogo.ReloadImage();
            }
            if (_companyData.Slogan != null) CompanySlogan.Text = _companyData.Slogan;
            CompanySlogan.IsVisible = !string.IsNullOrEmpty(_companyData.Slogan);

            //check if we should show the challenge
            if (_companyData.Challenges != null && _companyData.Challenges.Count > 0)
            {
                var hideDate = _companyData.Challenges[0].Finish.AddDays(_companyData.Challenges[0].DaysVisible);
                if (DateTime.Compare(hideDate, utcNow) <= 0)
                {
                    //do not show challenge
                    _registrationActive = false;
                    HideChallenge();
                }
                else
                {
                    //show challenge
                    await ShowChallenge(utcNow);
                }
            }
            else
            {
                //do not show challenge
                _registrationActive = false;
                HideChallenge();
            }
            StopLoading();
            IsBusy = false;
        }

        private void ForceHeights()
        {
            StepViewHeader.Height = StepViewNicknameHeader.Height;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            StopLoading();
        }

        private async void OnRegisterClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new CompanyDetailsPage_Register(_companyData, _registrationActive), true);
            ((View)sender).IsEnabled = true;

        }

        private void ShowRankingCategory(int index)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (_allRankingCategories[index].dropdowntitle.Contains("Alle Kollegen"))
                {
                    _userRankings[index].ForEach(x => x.ShowGender = true);
                }
                BindableLayout.SetItemsSource(StepView, _userRankings[index]);
            });
        }

        private async Task<bool> ShowChallenge(DateTime utcNow)
        {
            try
            {
                _rankingCategories = await _companyDataService.GetAllRankings(_companyData.Id);
                _dynamicRankingCategories = await _companyDataService.GetDynamicRankings(_companyData.Id);
                _allRankingCategories = new List<RankingCategory>();
                _allRankingCategories.AddRange(_rankingCategories);
                _allRankingCategories.AddRange(_dynamicRankingCategories);

                for (int i = 0; i < _allRankingCategories.Count; i++)
                {
                    if (i == 0)
                    {
                        _allRankingCategories[i].dropdowntitle = "| " + _allRankingCategories[i].dropdowntitle + " |";
                    }
                    else
                    {
                        _allRankingCategories[i].dropdowntitle = " " + _allRankingCategories[i].dropdowntitle + " |";
                    }
                }

                Console.WriteLine("got rankingCategories");
                Device.BeginInvokeOnMainThread(() =>
                {
                    ConfigureRankingButtons();
                });
                //we should add all Specific rankings to the _userRankings, but maybe at a later point?
                for (int i = 0; i < _rankingCategories.Count; i++)
                {
                    _userRankings.Add(await _companyDataService.GetSpecificRanking(_companyData.Id, _rankingCategories[i].Id));
                    //show first RankingCategory as fast as possible
                    if (i == 0) ShowRankingCategory(0);
                }


                for (int i = 0; i < _dynamicRankingCategories.Count; i++)
                {
                    _userRankings.Add(await _companyDataService.GetSpecificDynamicRanking(_companyData.Id, _dynamicRankingCategories[i].Id));
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            var start = _companyData.Challenges[0].Start;

            var entryStart = _companyData.Challenges[0].EntryStart;
            FirstTileLabel.Text = $"{entryStart.Day}.{entryStart.Month}.{entryStart.Year}";

            var finish = _companyData.Challenges[0].Finish;

            var entryStarted = DateTime.Compare(entryStart, utcNow) <= 0;
            var isRunning = DateTime.Compare(utcNow, start) > 0 && DateTime.Compare(utcNow, finish) <= 0;
            var stopped = DateTime.Compare(utcNow, finish) > 0;

            var dateFormat = "dd.MM.yyyy";

            if (!entryStarted)
            {
                FirstTileHeaderLabel.Text = "Anmeldung ab";
                FirstTileLabel.Text = entryStart.ToString(dateFormat);
                FirstTile.IsVisible = true;
                SecondTile.IsVisible = false;
                tileUnderRegistryFirst.IsVisible = false;
                tileUnderRegistrySecond.IsVisible = false;
            }
            else if (entryStarted)
            {
                FirstTile.IsVisible = true;
                SecondTile.IsVisible = true;
                tileUnderRegistryFirst.IsVisible = false;
                tileUnderRegistrySecond.IsVisible = false;

                FirstTileHeaderLabel.Text = "Die Anmeldung läuft noch";
                FirstTileLabel.Text = GetCounterString(start - utcNow);
                SecondTileHeaderLabel.Text = "Die Challenge startet am";
                SecondTileLabel.Text = start.ToString(dateFormat);
            }
            if (isRunning)
            {
                FirstTile.IsVisible = true;
                SecondTile.IsVisible = true;
                tileUnderRegistryFirst.IsVisible = false;
                tileUnderRegistrySecond.IsVisible = false;

                FirstTileHeaderLabel.Text = "Anmeldung abgeschlossen seit dem";
                FirstTileLabel.Text = start.ToString(dateFormat);
                SecondTileHeaderLabel.Text = "Die Challenge läuft noch";
                SecondTileLabel.Text = GetCounterString(finish - utcNow);
            }
            if (stopped)
            {
                _registrationActive = false;

                FirstTile.IsVisible = false;
                SecondTile.IsVisible = false;
                tileUnderRegistryFirst.IsVisible = true;
                tileUnderRegistrySecond.IsVisible = true;

                tileUnderRegistryFirstHeaderLabel.Text = "Anmeldung abgeschlossen seit dem";
                tileUnderRegistryFirstLabel.Text = start.ToString(dateFormat);
                tileUnderRegistrySecondHeaderLabel.Text = "Die Challenge ist abgelaufen seit dem";
                tileUnderRegistrySecondLabel.Text = finish.ToString(dateFormat);
            }

            return true;
        }

        private string GetCounterString(TimeSpan timeSpan)
        {
            var challengeRuntime = timeSpan;
            var challengeDays = challengeRuntime.ToString("%d");
            var challengeHours = challengeRuntime.ToString("%h");
            var challengeMinutes = challengeRuntime.ToString("%m");
            return $"{challengeDays} TAGE · {challengeHours} STUNDEN · {challengeMinutes} MINUTEN";
        }

        private void ConfigureRankingButtons()
        {
            BindableLayout.SetItemsSource(RankingsGrid, _allRankingCategories);
        }

        public void OnRankingCategoryClicked(object sender, EventArgs e)
        {
            var stackLayout = (View)sender;
            var id = (Label)stackLayout.FindByName("rankingId");
            for (int i = 0; i < _allRankingCategories.Count; i++)
            {
                if (_allRankingCategories[i].Id.ToString().Equals(id.Text))
                {
                    ShowRankingCategory(i);
                }
            }
        }

        private void HideChallenge()
        {
            ChallengeWrapper.IsVisible = false;
            NoActiveChallenge.IsVisible = true;
        }

        void StartLoading()
        {
            LoadingSpinner.IsRunning = true;
            LoadingSpinner.IsVisible = true;
        }
        async Task<bool> StopLoading()
        {
            LoadingSpinner.IsRunning = false;
            LoadingSpinnerLayout.IsVisible = false;
            return true;
        }
    }
}