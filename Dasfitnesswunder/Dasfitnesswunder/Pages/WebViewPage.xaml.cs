﻿using Dasfitnesswunder.Data;
using Dasfitnesswunder.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Dasfitnesswunder.Pages
{
    public enum Article
    {

    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WebViewPage : ContentPage
    {
        private string _titel { get; set; }
        private string _textType { get; set; }
        private int _companyId { get; set; }
        private readonly IGeneralService _generalService;
        private HtmlWebViewSource _htmlSource;

        public WebViewPage(string titel, int companyId, string textType)
        {
            InitializeComponent();
            _titel = titel;
            _textType = textType;
            _companyId = companyId;
            _generalService = Startup.ServiceProvider.GetService<IGeneralService>();
            _htmlSource = new HtmlWebViewSource { Html = string.Empty };
            ContentWebView.Source = _htmlSource;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            StartLoading();

            Title = _titel;

            if (string.Empty.Equals(_htmlSource.Html))
            {
                var articleId = await _generalService.GetSpecificArticleId(_companyId, _textType);

                if (articleId == -1)
                {
                    Console.WriteLine("could not load article id");
                    return;
                }
                var article = await _generalService.GetSpecificArticle(_companyId, articleId);
                if (article == null)
                {
                    Console.WriteLine("could not load article");
                    return;
                }

                article = article
                    .Replace("src=\"/", $"src=\"{Constants.BASEURL}/")
                    .Replace("<img", "<img style=\"width:100%; height:auto;\" ")
                    .Replace("href=\"https://flow.polar.com\" target=\"_blank\" rel=\"noopener\"", "href =\"https://flow.polar.com\"")
                    ;

                if (Device.RuntimePlatform == Device.iOS)
                {
                    article = HtmlHelper.AddiOSStylingHeader(article);
                }

                _htmlSource.Html = article;

                ContentWebView.Navigating += async (s, e) =>
                {
                    var uri = new Uri(e.Url);
                    if (!uri.ToString().Contains("file:///"))
                    {
                        e.Cancel = true;
                    }
                    if (await Xamarin.Essentials.Launcher.CanOpenAsync(uri))
                    {
                        await Xamarin.Essentials.Launcher.OpenAsync(uri);
                    }
                };
            }

            StopLoading();
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            StopLoading();
        }
        void StartLoading()
        {
            LoadingSpinner.IsRunning = true;
            LoadingSpinner.IsVisible = true;
        }
        void StopLoading()
        {
            LoadingSpinner.IsRunning = false;
            LoadingSpinnerLayout.IsVisible = false;
        }
    }
}