﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Dasfitnesswunder.Data;
using Dasfitnesswunder.Helpers;
using Dasfitnesswunder.Model;
using Microsoft.Extensions.DependencyInjection;
using Xamarin.Forms;

namespace Dasfitnesswunder.Pages
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private readonly ICompanyDataService _companyDataService;
        private readonly IGeneralService _generalDataService;

        #region footer
        protected int _companyId = 1;
        private async void OnImpressumClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Impressum, _companyId, Constants.Impressum), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnPrivacyClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Datenschutz, _companyId, Constants.Datenschutz), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnConditionsClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Teilnahmebedingungen, _companyId, Constants.Teilnahmebedingungen), true);
            ((View)sender).IsEnabled = true;

        }
        #endregion

        public MainPage()
        {
            InitializeComponent();

            _companyDataService = Startup.ServiceProvider.GetService<ICompanyDataService>();
            _generalDataService = Startup.ServiceProvider.GetService<IGeneralService>();
            //Title = "Home";
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            IsBusy = true;

            StartLoading();

            var col = 0;
            var row = 0;

            //foreach (var child in CompanyGrid.Children)
            //{
            //    var canvasView = (SkiaSharp.Views.Forms.SKCanvasView)child;
            //    child.IsVisible = false;
            //}

            CompanyGrid.Children.Clear();
            CompanyGrid.RowDefinitions.Clear();

            var companySmallList = (await _companyDataService.GetAll()).Where(x => x.Show).ToList();            

#if DEBUG
            if (Constants.BASEURL.Equals("https://www.dasfitnesswunder.de"))
            {
                companySmallList.Insert(0, new CompanySmall { CompanyName = "TEST", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, Id = 10018 });
            }
            else if (Constants.BASEURL.Equals("https://www.kollegen-cup.de"))
            {
                companySmallList.Insert(0, new CompanySmall { CompanyName = "TEST", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now, Id = 10011 });
            }
            Console.WriteLine("Debug version");
#endif

            var firstCompanies = new List<Company>();

            foreach (var companySmall in companySmallList.Take(10))
                firstCompanies.Add(await _companyDataService.GetSpecific(companySmall.Id));

            for (var i = 0; i < firstCompanies.Count(); i += 2)
            {
                var newRowDefinition = new RowDefinition
                {
                    Height = 35
                };

                CompanyGrid.RowDefinitions.Add(newRowDefinition);
            }

            foreach (var firstCompany in firstCompanies)
            {
                using (var httpClient = new System.Net.Http.HttpClient())
                {
                    var image = new FFImageLoading.Forms.CachedImage
                    {
                        Source = $"{Constants.BASEURL}{firstCompany.Logo}"
                    };

                    if (BitmapHelper.IsChallengeExpired(firstCompany))
                    {
                        image.Transformations.Add(new FFImageLoading.Transformations.GrayscaleTransformation());
                    }

                    if (col == 1)
                    {
                        image.HorizontalOptions = LayoutOptions.End;
                    }
                    else
                    {
                        image.HorizontalOptions = LayoutOptions.Start;
                    }

                    var tapGestureRecognizer = new TapGestureRecognizer
                    {
                        Command = new Command(async () =>
                        {
                            var gr = image.GestureRecognizers.FirstOrDefault();
                            image.GestureRecognizers.Clear();
                            await Navigation.PushAsync(new CompanyDetailsPage(firstCompany), true);
                            image.GestureRecognizers.Add(gr);
                        })
                    };
                    image.GestureRecognizers.Clear();
                    image.GestureRecognizers.Add(tapGestureRecognizer);
                    CompanyGrid.Children.Add(image);

                    Grid.SetRow(image, row);
                    Grid.SetColumn(image, col);

                    //var bytes = await httpClient.GetByteArrayAsync($"{Constants.BASEURL}{firstCompany.Logo}");

                    //CustomSKCanvasView canvasView = new CustomSKCanvasView()
                    //{
                    //    GrayScale = BitmapHelper.IsChallengeExpired(firstCompany),
                    //    Start = col == 0,
                    //    Source = bytes
                    //};

                    //canvasView.PaintSurface += BitmapHelper.OnCanvasViewPaintSurface;

                    //var tapGestureRecognizer = new TapGestureRecognizer
                    //{
                    //    Command = new Command(async () =>
                    //    {
                    //        var gr = canvasView.GestureRecognizers.FirstOrDefault();
                    //        canvasView.GestureRecognizers.Clear();
                    //        await Navigation.PushAsync(new CompanyDetailsPage(firstCompany), true);
                    //        canvasView.GestureRecognizers.Add(gr);
                    //    })
                    //};

                    //canvasView.GestureRecognizers.Add(tapGestureRecognizer);
                    //CompanyGrid.Children.Add(canvasView);

                    //Grid.SetRow(canvasView, row);
                    //Grid.SetColumn(canvasView, col);

                    //canvasView.InvalidateSurface();
                }
                if (col == 1) row++;
                col = col == 0 ? 1 : 0;
            }
            StopLoading();
            IsBusy = false;

            //var sas = Xamarin.Forms.DependencyService.Get<IStartActivityService>();

            //sas.StartThryveLoginActivity(1, 1, 1);
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            StopLoading();
        }

        private async void OnAllCompanyFrameClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new CompaniesPage(), true);
            ((View)sender).IsEnabled = true;

        }

        private async void OnAboutImageClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new AboutPage(), true);
            ((View)sender).IsEnabled = true;

        }

        //private async void OnHowImageClicked(object sender, EventArgs e)
        //{
        //    //await Navigation.PushAsync(new HowPage());
        //    throw new NotImplementedException();
        //}

        private async void OnFaqImageClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new ContactPage(), true);
            ((View)sender).IsEnabled = true;

        }

        void StartLoading()
        {
            LoadingSpinner.IsRunning = true;
            LoadingSpinner.IsVisible = true;
            LoadingLayout.IsVisible = true;
        }
        void StopLoading()
        {
            LoadingSpinner.IsRunning = false;
            LoadingSpinner.IsVisible = false;
            LoadingLayout.IsVisible = false;
        }
    }
}