
using Dasfitnesswunder.Data;
using Dasfitnesswunder.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.Xaml;

namespace Dasfitnesswunder.Pages
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompanyDetailsPage_Register : ContentPage
    {
        private Company _companyData;
        private readonly IGeneralService _generalService;
        private bool _challengeActive;
        private bool _startedLogin;
        private static int LogoSize = 60;

        public static bool RegistrationCompleteSuccessfully = false;

        #region footer
        protected int _companyId = 1;
        private async void OnImpressumClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Impressum, _companyId, Constants.Impressum), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnPrivacyClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Datenschutz, _companyId, Constants.Datenschutz), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnConditionsClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Teilnahmebedingungen, _companyId, Constants.Teilnahmebedingungen), true);
            ((View)sender).IsEnabled = true;

        }
        #endregion

        public CompanyDetailsPage_Register(Company companyData, bool challengeActive)
        {
            InitializeComponent();
            _challengeActive = challengeActive;
            _generalService = Startup.ServiceProvider.GetService<IGeneralService>();
            _companyData = companyData;

            CompanyLogo.Success += (sender, e) =>
            {
                var height = e.ImageInformation.OriginalHeight;
                var width = e.ImageInformation.OriginalWidth;

                var ratio = (double)LogoSize / (double)height;
                CompanyLogo.HeightRequest = LogoSize;
                CompanyLogo.WidthRequest = width * ratio;
            };

            if (_companyData != null && _companyData.registration != null && _companyData.registration.api_source != null && _companyData.registration.api_source.Count > 0)
            {
                /* TODO remove when connection is working ====================================== */
                _companyData.registration.api_source.RemoveAll(x => x.source_id == 5 || x.source_id == 6);
                /* TODO remove when connection is working ====================================== */

                if (Device.RuntimePlatform == Device.Android)
                {
                    _companyData.registration.api_source.RemoveAll(x => x.source_id == 5);
                }
                else
                {
                    _companyData.registration.api_source.RemoveAll(x => x.source_id == 6);
                }
            }

            _companyId = companyData.Id;
            if (_companyData.registration.dropdowns.Count > 0)
            {
                AbteilungsChallengePicker.ItemsSource = _companyData.registration.dropdowns.FirstOrDefault().dropdownvalues;
            }
            TrackerPicker.ItemsSource = _companyData.registration.api_source;


            RegistrySendButton.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    OnRegisterClicked();
                })
            }
            );
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_companyData == null) return;

            //set Logo and Slogan
            if (_companyData.Logo != null)
            {
                CompanyLogo.Source = $"{Constants.BASEURL}{_companyData.Logo}";
                CompanyLogo.ReloadImage();
            }
            if (_companyData.Slogan != null) CompanySlogan.Text = _companyData.Slogan;

            NickNameLabel.Text = $"Du meldest Dich mit Deinen pers�nlichen Daten hier bei Eurer Challenge von {Constants.APPNAME} an. Nur Dein Nickname wird sp�ter angezeigt.";

            ForceGridHeights();

            if (!_challengeActive)
            {
                NotActiveMessage.IsVisible = true;
                RegistrationForm.IsVisible = false;
            }

            if (!_startedLogin)
            {
                RegistrationCompleteSuccessfully = false;
            }

            if (_startedLogin && RegistrationCompleteSuccessfully)
            {
                Task.Run(() =>
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        foreach (var item in Navigation.NavigationStack.ToList())
                        {
                            if (!(item is MainPage))
                            {
                                Navigation.RemovePage(item);
                            }
                        }
                        await Navigation.PushAsync(new Success(_companyId), true);
                    });
                });
            }
#if DEBUG
            AddTestButtons();
#endif
        }

        //forcing grid heights because all other methods of determining grid height have failed
        private void ForceGridHeights()
        {
            var children = PickerGrid.Children.ToList();
            PickerGrid.HeightRequest = children[0].Height + children[2].Height + 12;

            children = StepsGrid.Children.ToList();
            StepsGrid.HeightRequest = children[0].Height + children[2].Height + children[4].Height + 12;
        }

        async void OnRegisterClicked()
        {
            var vorname = VornameEntry.Text;
            var rnd = new Random();
            var rndInt = rnd.Next(0, 1000000);

            //verify inputs
            if (true
            //AnredePicker.SelectedIndex != -1
            //&& AbteilungsChallengePicker.SelectedIndex != -1
            ////&& !string.IsNullOrEmpty(NicknameEntry.Text)
            ////&& !string.IsNullOrEmpty(FamiliennameEntry.Text)
            ////&& !string.IsNullOrEmpty(VornameEntry.Text)
            ////&& !string.IsNullOrEmpty(EmailEntry.Text)
            ////&& UsingTrackerBox.IsChecked
            ////&& AcceptConditionsBox.IsChecked
            //&& TrackerPicker.SelectedIndex != -1
            )
            //correct inputs
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ErrorLabel.IsVisible = false;
                });

                var abteilung = _companyData.registration.dropdowns.FirstOrDefault().dropdownvalues;

                var createUserResponse = await _generalService.CreateUser(
                    companyID: _companyData.Id,
                    gender: AnredePicker.SelectedIndex == -1 ? null : (int?)AnredePicker.SelectedIndex,
                    dropdownValueID: AbteilungsChallengePicker.SelectedIndex == -1 ? null : (int?)abteilung[AbteilungsChallengePicker.SelectedIndex].id,
                    nickName: NicknameEntry.Text,
                    lastName: FamiliennameEntry.Text,
                    firstName: VornameEntry.Text,
                    email: EmailEntry.Text,
                    polarRegConfirm: UsingTrackerBox.IsChecked,
                    agbConfirm: AcceptConditionsBox.IsChecked,
                    apiSourceID: TrackerPicker.SelectedIndex == -1 ? null : (int?)_companyData.registration.api_source[TrackerPicker.SelectedIndex].source_id
                );
                int userID = -1;

                if (createUserResponse != null /*&& createUserResponse.Any()*/)
                {
                    //if (int.TryParse(createUserResponse[0], out userID))
                    if (createUserResponse.Id != -1)
                    {
                        //success in creating user               
                        StartLoginActivity(userID, _companyData.registration.api_source[TrackerPicker.SelectedIndex].source_id);
                        HideError();
                        //ClearForm();
                    }
                    else
                    {
                        //error with list of strings as error notes
                        ShowError(createUserResponse);
                    }
                }
                else
                {
                    //error with no response
                    ShowError();
                }
            }
            else
            {
                ShowError();
            }
        }

        public void CallOnAppering()
        {
            OnAppearing();
        }

        async void OnTestRegisterClicked(object sender, EventArgs args)
        {
            var rnd = new Random();
            var rndInt = rnd.Next(0, 1000000);
            var selectedTracker = TrackerPicker.SelectedIndex == -1 ? 1 : TrackerPicker.SelectedIndex;


            var createUserResponse = await _generalService.CreateUser(
                    companyID: _companyData.Id,
                    gender: 0,
                    dropdownValueID: 186,
                    nickName: $"TEST - Best Runner {rndInt}",
                    lastName: $"TEST - Mustermann {rndInt}",
                    firstName: $"TEST - Max {rndInt}",
                    email: $"TEST-bestrunner{rndInt}@gmail.com",
                    polarRegConfirm: true,
                    agbConfirm: true,
                    apiSourceID: _companyData.registration.api_source[selectedTracker].source_id
                );
            int userID = -1;

            if (createUserResponse != null /*&& createUserResponse.Any()*/)
            {
                //if (int.TryParse(createUserResponse[0], out userID))
                if (createUserResponse.Id != -1)
                {
                    //success in creating user
                    StartLoginActivity(userID, _companyData.registration.api_source[selectedTracker].source_id);
                    HideError();
                    //ClearForm();
                }
                else
                {
                    //error with list of strings as error notes
                    ShowError(createUserResponse);
                }
            }
            else
            {
                //error with no response
                ShowError();
            }
        }

        async void OnTestRegisterFailClicked(object sender, EventArgs args)
        {
            var selectedTracker = TrackerPicker.SelectedIndex == -1 ? 1 : TrackerPicker.SelectedIndex;

            var rnd = new Random();
            var rndInt = rnd.Next(0, 1000000);

            //StartLoginActivity(1783, 2);

            //await _generalService.CreateUser(
            //        companyID: _companyData.Id,
            //        gender: 0,
            //        dropdownValueID: 186,
            //        nickName: $"TEST - Best Runner",
            //        lastName: $"TEST - Mustermann {rndInt}",
            //        firstName: $"TEST - Max {rndInt}",
            //        email: $"TEST-bestrunner{rndInt}@gmail.com",
            //        polarRegConfirm: true,
            //        agbConfirm: true,
            //        apiSourceID: 2
            //    );

            var createUserResponse = await _generalService.CreateUser(
                    companyID: _companyData.Id,
                    gender: 0,
                    dropdownValueID: 186,
                    nickName: $"TEST - Best Runner {rndInt}",
                    lastName: $"TEST - Mustermann {rndInt}",
                    firstName: $"TEST - Max {rndInt}",
                    email: $"TEST-bestrunner@gmail.com",
                    polarRegConfirm: true,
                    agbConfirm: true,
                    apiSourceID: _companyData.registration.api_source[selectedTracker].source_id
                );
        }


        private void StartLoginActivity(int userID, int sourceID)
        {
            var startActivity = DependencyService.Get<IStartActivityService>();

            if (startActivity != null)
            {
                this._startedLogin = true;
                startActivity.Caller = this;
                startActivity.StartThryveLoginActivity(_companyData.Id, userID, sourceID);
            }
        }

        private void HideError()
        {
            ResetColors();
            Device.BeginInvokeOnMainThread(() =>
            {
                ErrorFrame.IsVisible = false;
            });
        }

        private void ResetColors()
        {
            var neutralColor = Color.Black;
            Device.BeginInvokeOnMainThread(() =>
            {
                AnredeText.TextColor = neutralColor; AnredePicker.TitleColor = neutralColor;
                VornameText.TextColor = neutralColor; VornameEntry.PlaceholderColor = neutralColor;
                FamiliennameText.TextColor = neutralColor; FamiliennameEntry.PlaceholderColor = neutralColor;
                NicknameText.TextColor = neutralColor; NicknameEntry.PlaceholderColor = neutralColor;
                EmailText.TextColor = neutralColor; EmailEntry.PlaceholderColor = neutralColor;
                AbteilungsChallengeText.TextColor = neutralColor; AbteilungsChallengePicker.TitleColor = neutralColor;
                UsingTrackerBox.Color = neutralColor; UsingTrackerText.TextColor = neutralColor;
                AcceptConditionsBox.Color = neutralColor; AcceptConditionsText.TextColor = neutralColor;
                TrackerText.TextColor = neutralColor; TrackerPicker.TitleColor = neutralColor;
            });
        }

        private void ClearForm()
        {
            AnredePicker.SelectedIndex = -1;
            AbteilungsChallengePicker.SelectedIndex = -1;
            NicknameEntry.Text = "";
            FamiliennameEntry.Text = "";
            VornameEntry.Text = "";
            EmailEntry.Text = "";
            UsingTrackerBox.IsChecked = false;
            AcceptConditionsBox.IsChecked = false;
        }

        private void ShowError()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                ErrorFrame.IsVisible = true;
                ErrorLabel.Text = "Bitte das Formular vollst�ndig ausf�llen.";
                ErrorLabel.IsVisible = true;
            });
        }

        private void ShowError(CreateUserResponse response)
        {
            ResetColors();

            Device.BeginInvokeOnMainThread(() =>
            {
                var errorColor = Color.FromHex("A94442");

                if (!response.GenderSet) { AnredeText.TextColor = errorColor; AnredePicker.TitleColor = errorColor; }
                if (!response.FirstnameSet) { VornameText.TextColor = errorColor; VornameEntry.PlaceholderColor = errorColor; }
                if (!response.LastnameSet) { FamiliennameText.TextColor = errorColor; FamiliennameEntry.PlaceholderColor = errorColor; }
                if (!response.NicknameSet) { NicknameText.TextColor = errorColor; NicknameEntry.PlaceholderColor = errorColor; }
                if (!response.EmailSet) { EmailText.TextColor = errorColor; EmailEntry.PlaceholderColor = errorColor; }
                if (!response.Userdropdowns_dropdownvalueSet) { AbteilungsChallengeText.TextColor = errorColor; AbteilungsChallengePicker.TitleColor = errorColor; }
                if (!response.Polar_reg_confirmSet) { UsingTrackerBox.Color = errorColor; UsingTrackerText.TextColor = errorColor; }
                if (!response.Agb_confirmSet) { AcceptConditionsBox.Color = errorColor; AcceptConditionsText.TextColor = errorColor; }
                if (!response.BaseSet) { TrackerText.TextColor = errorColor; TrackerPicker.TitleColor = errorColor; }

                ErrorFrame.IsVisible = true;
                var errorText = string.Join("\n", response.Messages);
                ErrorLabel.Text = errorText;
                ErrorLabel.IsVisible = true;
            });
        }

        private void AnredePicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            VornameEntry.Focus();
        }

#if DEBUG
        private void AddTestButtons()
        {
            //< Button
            //            x: Name = "TestRegisterSuccessButton"
            //            IsVisible = "false"
            //            IsEnabled = "false"
            //            Text = "Test Register"
            //            VerticalOptions = "CenterAndExpand"
            //            HorizontalOptions = "Center"
            //            Clicked = "OnTestRegisterClicked" ></ Button >

            //        < Button
            //            x: Name = "TestRegisterFailButton"
            //            IsVisible = "false"
            //            IsEnabled = "false"
            //            Text = "Test Register Fail"
            //            VerticalOptions = "CenterAndExpand"
            //            HorizontalOptions = "Center"
            //            Clicked = "OnTestRegisterFailClicked" ></ Button >
            Button successButton = new Button()
            {
                Text = "Test Register Success",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            successButton.Clicked += OnTestRegisterClicked;
            RegistrationLayout.Children.Add(successButton);

            Button failButton = new Button()
            {
                Text = "Test Register Fail",
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            failButton.Clicked += OnTestRegisterFailClicked;
            RegistrationLayout.Children.Add(failButton);
        }
#endif
    }
}