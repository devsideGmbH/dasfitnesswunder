﻿using Dasfitnesswunder.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Dasfitnesswunder.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactPage : ContentPage
    {
        private readonly IGeneralService _generalService;

        #region footer
        protected int _companyId = 1;
        private async void OnImpressumClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Impressum, _companyId, Constants.Impressum), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnPrivacyClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Datenschutz, _companyId, Constants.Datenschutz), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnConditionsClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Teilnahmebedingungen, _companyId, Constants.Teilnahmebedingungen), true);
            ((View)sender).IsEnabled = true;

        }
        #endregion

        public ContactPage()
        {
            InitializeComponent();

            _generalService = Startup.ServiceProvider.GetService<IGeneralService>();

            if (Device.RuntimePlatform == Device.iOS) MessageEditorFrame.BackgroundColor = Color.FromHex("#F3F3F3");
            ContactSendButton.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    OnSendClicked();
                })
            }
            );
        }

        async void OnSendClicked()
        {
            var email = new EmailAddressAttribute();
            //verify inputs
            //we use backendverification cause it exist and gives good feedback
            //if (
            ////!string.IsNullOrEmpty(VornameEntry.Text)
            ////&& !string.IsNullOrEmpty(NachnameEntry.Text
            ////&& !string.IsNullOrEmpty(EmailEntry.Text)
            ////&& !string.IsNullOrEmpty(MessageEditor.Text)
            ////&& email.IsValid(EmailEntry.Text)
            //)
            //{
            //valid inputs
            var result = await _generalService.SendInfoRequest(NachnameEntry.Text, VornameEntry.Text, EmailEntry.Text, MessageEditor.Text, PhoneEntry.Text, FirmaEntry.Text);
            if (result == null)
            {
                //error with no specific information
                ShowError();
            }
            else if (!result.Success)
            {
                //error with specific information that needs to be displayed
                ShowError(result);
            }
            else
            {
                //success
                HideError();
                ClearForm();
                Navigation.PopAsync();
            }


            //}
            //else
            //{
            //    //invalid inputs
            //    ShowError();
            //}
        }

        private void ResetColors()
        {
            var neutralColor = Color.Black;
            Device.BeginInvokeOnMainThread(() =>
            {
                VornameText.TextColor = neutralColor; VornameEntry.PlaceholderColor = neutralColor;
                NachnameText.TextColor = neutralColor; NachnameEntry.PlaceholderColor = neutralColor;
                EmailText.TextColor = neutralColor; EmailEntry.PlaceholderColor = neutralColor;
                MessageText.TextColor = neutralColor; MessageEditor.PlaceholderColor = neutralColor;
            });
        }

        private void HideError()
        {
            ResetColors();

            Device.BeginInvokeOnMainThread(() =>
            {
                ErrorFrame.IsVisible = false;
            });
        }

        private void ClearForm()
        {
            VornameEntry.Text = "";
            NachnameEntry.Text = "";
            FirmaEntry.Text = "";
            EmailEntry.Text = "";
            PhoneEntry.Text = "";
            MessageEditor.Text = "";
        }

        private void ShowError()
        {
            ResetColors();

            Device.BeginInvokeOnMainThread(() =>
            {
                ErrorFrame.IsVisible = true;
                ErrorLabel.Text = "Bitte das Formular vollständig ausfüllen.";
            });
        }

        private void ShowError(Model.InfoRequestResponse response)
        {
            ResetColors();

            Device.BeginInvokeOnMainThread(() =>
            {
                ErrorFrame.IsVisible = true;
                var errorText = string.Join("\n", response.Messages);
                ErrorLabel.Text = errorText;

                var errorColor = Color.FromHex("A94442");

                if (!response.FirstnameSet) { VornameText.TextColor = errorColor; VornameEntry.PlaceholderColor = errorColor; }
                if (!response.LastnameSet) { NachnameText.TextColor = errorColor; NachnameEntry.PlaceholderColor = errorColor; }
                if (!response.EmailSet) { EmailText.TextColor = errorColor; EmailEntry.PlaceholderColor = errorColor; }
                if (!response.MessageSet) { MessageText.TextColor = errorColor; MessageEditor.PlaceholderColor = errorColor; }
            });
        }

        private void MessageEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (MessageEditor.Height < 120)
            {
                MessageEditor.HeightRequest = 120;
            }
            else
            {
                MessageEditor.HeightRequest = -1;
            }
        }
    }
}