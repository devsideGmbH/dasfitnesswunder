﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using FFImageLoading.Transformations;
using Dasfitnesswunder.Data;
using Dasfitnesswunder.Helpers;
using Dasfitnesswunder.Model;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Dasfitnesswunder.BitmapExtensions;

namespace Dasfitnesswunder.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompaniesPage : ContentPage
    {
        private readonly ICompanyDataService _companyDataService;
        private List<CompanySmall> _companySmallList;
        private List<Company> _firstCompanies;

        SKBitmap _bitmap;
        #region footer
        protected int _companyId = 1;
        private string _searchText = "";

        private double oldWidht;
        private double oldHeight;
        private int _columnsCount;

        private async void OnImpressumClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Impressum, _companyId, Constants.Impressum), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnPrivacyClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Datenschutz, _companyId, Constants.Datenschutz), true);
            ((View)sender).IsEnabled = true;

        }
        private async void OnConditionsClicked(object sender, EventArgs e)
        {
            ((View)sender).IsEnabled = false;
            await Navigation.PushAsync(new WebViewPage(Constants.Teilnahmebedingungen, _companyId, Constants.Teilnahmebedingungen), true);
            ((View)sender).IsEnabled = true;

        }
        #endregion

        public CompaniesPage()
        {
            InitializeComponent();

            _companyDataService = Startup.ServiceProvider.GetService<ICompanyDataService>();
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width != this.oldWidht || height != this.oldHeight)
            {
                this.oldWidht = width;
                this.oldHeight = height;

                this._columnsCount = width > height ? 3 : 2;
                if (_firstCompanies != null)
                {
                    FillCompanyGrid(_searchText, _columnsCount);
                }
            }
        }

        private void SetUpCompanyGridColumns(int columns)
        {
            CompanyGrid.RowDefinitions.Clear();
            CompanyGrid.ColumnDefinitions.Clear();
            CompanyGrid.Children.Clear();

            for (int i = 0; i < columns; i++)
            {
                CompanyGrid.ColumnDefinitions.Add(new ColumnDefinition
                {
                    Width = new GridLength(35, GridUnitType.Star)
                });
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            IsBusy = true;

            if (_companySmallList == null || _companySmallList.Count == 0)
            {
                await GetCompanies();
            }

            FillCompanyGrid(_searchText, _columnsCount);

            IsBusy = false;
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            StopLoading();
        }
        private async Task GetCompanies()
        {
            _companySmallList = await _companyDataService.GetAll();
            _firstCompanies = new List<Company>();

            using (var httpClient = new System.Net.Http.HttpClient())
                foreach (var companySmall in _companySmallList)
                {
                    //filter out "master vorlagen" and "Hauptseite"
                    if (
#if !DEBUG
                    companySmall.Show && 
#endif
                    companySmall.Id != 1 && companySmall.Id != 9999)
                    {
                        var company = await _companyDataService.GetSpecific(companySmall.Id);
                        //company.Source = await httpClient.GetByteArrayAsync($"{Constants.BASEURL}{company.Logo}");
                        _firstCompanies.Add(company);
                    }
                }

            //var TEST = _firstCompanies.Select(x => x.CompanyName).ToList();

        }

        //private async Task<bool> FillCompanyGrid(string searchString)
        private void FillCompanyGrid(string searchString, int columnsCount)
        {
            StartLoading();
            SetUpCompanyGridColumns(_columnsCount);

            var col = 0;
            var row = 0;

            var companiesToSearch = _firstCompanies.Where(x => x.CompanyName.ToLower().Contains(searchString.ToLower())).ToList();

            var numberOfCompanies = companiesToSearch.Count;
            for (var i = 0; i < numberOfCompanies; i += columnsCount)
            {
                var newRowDefinition = new RowDefinition
                {
                    Height = new GridLength(35, GridUnitType.Star)
                };

                CompanyGrid.RowDefinitions.Add(newRowDefinition);
            }


            foreach (var firstCompany in companiesToSearch)
            {
                CachedImage image = new CachedImage
                {
                    Source = $"{Constants.BASEURL}{firstCompany.Logo}",
                    VerticalOptions = LayoutOptions.StartAndExpand
                };

                if (BitmapHelper.IsChallengeExpired(firstCompany))
                {
                    image.Transformations.Add(new GrayscaleTransformation());
                } 
                if (col == columnsCount - 1)
                {
                    image.HorizontalOptions = LayoutOptions.End;
                }
                else
                {
                    image.HorizontalOptions = LayoutOptions.Start;
                }

                var tapGestureRecognizer = new TapGestureRecognizer
                {
                    Command = new Command(async () =>
                    {
                        var gr = image.GestureRecognizers.FirstOrDefault();
                        image.GestureRecognizers.Clear();
                        await Navigation.PushAsync(new CompanyDetailsPage(firstCompany), true);
                        image.GestureRecognizers.Add(gr);
                    })
                };
                image.GestureRecognizers.Clear();
                image.GestureRecognizers.Add(tapGestureRecognizer);


                //CustomSKCanvasView canvasView = new CustomSKCanvasView()
                //{
                //    GrayScale = BitmapHelper.IsChallengeExpired(firstCompany),
                //    Start = col != columnsCount - 1,
                //    Source = firstCompany.Source,
                //    BackgroundColor = Color.FromHex("#FC"),
                //    Margin = new Thickness(0, -41, 0, 0),
                //    HeightRequest = 35
                //};

                //canvasView.PaintSurface += BitmapHelper.OnCanvasViewPaintSurface;

                //var tapGestureRecognizer = new TapGestureRecognizer
                //{
                //    Command = new Command(async () =>
                //    {
                //        var gr = canvasView.GestureRecognizers.FirstOrDefault();
                //        canvasView.GestureRecognizers.Clear();
                //        await Navigation.PushAsync(new CompanyDetailsPage(firstCompany), true);
                //        canvasView.GestureRecognizers.Add(gr);
                //    })
                //};

                //canvasView.GestureRecognizers.Add(tapGestureRecognizer);

                ////give the skcanvasview a label as background, otherwise it might mot fully overwrite old skcanvasviews
                //var absoluteLayout = new StackLayout()
                //{
                //    BackgroundColor = Color.FromHex("#FCFCFC"),
                //    HorizontalOptions = LayoutOptions.Fill,
                //    HeightRequest = 35,
                //    Padding = new Thickness(0),
                //    Margin = new Thickness(0),
                //    Children =
                //        {
                //            new Label()
                //            {
                //                BackgroundColor = Color.FromHex("#FCFCFC"),
                //                HorizontalOptions = LayoutOptions.FillAndExpand,
                //                VerticalOptions = LayoutOptions.FillAndExpand,
                //                HeightRequest = 35,
                //            },
                //            canvasView
                //        }
                //};

                CompanyGrid.Children.Add(image);

                Grid.SetRow(image, row);
                Grid.SetColumn(image, col);
                if (col == columnsCount - 1)
                {
                    row++;
                    col = 0;
                }
                else
                {
                    col++;
                }
            }
            StopLoading();
        }



        void SearchCompanies(object sender, EventArgs e)
        {
            if (e == null) return;
            var searchEventArgs = (TextChangedEventArgs)e;
            var newText = (searchEventArgs.NewTextValue ?? "").Trim();
            var oldText = (searchEventArgs.OldTextValue ?? "").Trim();

            if (!newText.Equals(oldText))
            {
                _searchText = newText;
                FillCompanyGrid(newText, _columnsCount);
            }
        }

        void StartLoading()
        {
            LoadingSpinner.IsRunning = true;
            LoadingSpinner.IsVisible = true;
            LoadingLayout.IsVisible = true;
        }
        void StopLoading()
        {
            LoadingSpinner.IsRunning = false;
            LoadingSpinner.IsVisible = false;
            LoadingLayout.IsVisible = false;
        }
    }
}