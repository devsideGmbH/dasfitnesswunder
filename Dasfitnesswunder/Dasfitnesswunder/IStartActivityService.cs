﻿using Dasfitnesswunder.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dasfitnesswunder
{
    public interface IStartActivityService
    {
        void StartThryveLoginActivity(int companyId, int userId, int thryveSourceId);

        CompanyDetailsPage_Register Caller { get; set; }
    }
}
