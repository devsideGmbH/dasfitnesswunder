﻿using Dasfitnesswunder.Data;
using Dasfitnesswunder.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Dasfitnesswunder
{
    public static class DependencyInjectionContainer
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddSingleton<IRestService, RestService>(service => new RestService(Constants.RESTAPIKEY));

            services.AddTransient<ICompanyDataService, CompanyDataService>();
            services.AddTransient<IGeneralService, GeneralService>();

            return services;
        }
    }
}