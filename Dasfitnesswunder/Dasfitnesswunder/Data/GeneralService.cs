﻿using Dasfitnesswunder.Model;
using Dasfitnesswunder.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Dasfitnesswunder.Data
{
    public class GeneralService : IGeneralService
    {
        private readonly IRestService _restService;

        private JsonSerializerSettings _jsonSerializerSettings;

        public GeneralService(IRestService restService)
        {
            _restService = restService;
            _jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                }
            };
        }

        public async Task<bool> SetThryveToken(int companyID, int userID, string thryveToken)
        {
            try
            {
                var (content, status) = await _restService.PutCall($"/api/v1/tenants/{companyID}/users/{userID}/settoken", JsonConvert.SerializeObject(new { thryve_access_token = thryveToken }));
                return status == HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return false;
        }
        public async Task<CreateUserResponse> CreateUser(int companyID, int? gender, int? dropdownValueID,/* DateTime dayOfBirth, */string nickName, string lastName, string firstName, string email, bool polarRegConfirm, bool agbConfirm, int? apiSourceID)
        {
            try
            {
                var body = new CreateUserRequest()
                {
                    gender = gender.ToString(),
                    dayofbirth = "01.01.1980",
                    nickname = nickName,
                    lastname = lastName,
                    firstname = firstName,
                    email = email,
                    polar_reg_confirm = polarRegConfirm,
                    agb_confirm = agbConfirm,
                    api_source_id = apiSourceID.ToString()
                };
                body.userdropdowns_attributes.Add(0, new Dictionary<string, string>() { { "dropdownvalue_id", dropdownValueID.ToString() } });

                var (content, status) = await _restService.PostCall($"/api/v1/tenants/{companyID}/users", JsonConvert.SerializeObject(body));

                var ret = new CreateUserResponse();
                //List<string> ret = new List<string>();

                //successfully created User
                if (status == HttpStatusCode.OK && content != null)
                {
                    var jArray = JArray.Parse(content);
                    ret.Id = jArray.First.ToObject<JObject>().Value<int>("id");
                    //ret.Messages.Add(jArray.First.ToObject<JObject>().Value<string>("id"));
                    return ret;
                }
                //errpr when creating User
                else if (status != HttpStatusCode.OK && content != null)
                {
                    JObject errorList = JObject.Parse(content);

                    foreach (var prop in errorList.Properties())
                    {
                        ret.Messages.Add($"\u2022 {prop.Value[0]}");

                        //check which inputs are not set
                        switch (prop.Name)
                        {
                            case "firstname": ret.FirstnameSet = false; break;
                            case "lastname": ret.LastnameSet = false; break;
                            case "email": ret.EmailSet = false; break;
                            case "nickname": ret.NicknameSet = false; break;
                            case "polar_reg_confirm": ret.Polar_reg_confirmSet = false; break;
                            case "agb_confirm": ret.Agb_confirmSet = false; break;
                            case "gender": ret.GenderSet = false; break;
                            case "dayofbirth": ret.DayofbirthSet = false; break;
                            case "base": ret.BaseSet = false; break;
                            case "userdropdowns.dropdownvalue": ret.Userdropdowns_dropdownvalueSet = false; break;
                            default: break;
                        }
                    }

                    return ret;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return null;
        }

        public async Task<InfoRequestResponse> SendInfoRequest(string lastName, string firstName, string email, string message, string phone = "", string company = "")
        {
            try
            {
                var infoRequest = new InfoRequest()
                {
                    phone = phone,
                    lastname = lastName,
                    firstname = firstName,
                    company = company,
                    email = email,
                    message = message
                };

                var (content, status) = await _restService.PostCall($"/api/v1/inforequests", JsonConvert.SerializeObject(infoRequest));

                //List<string> ret = new List<string>();
                var ret = new InfoRequestResponse();

                if (status == HttpStatusCode.OK && content != null)
                {
                    ret.Success = true;
                    ret.Messages.Add("OK");
                    return ret;
                }
                else if (status != HttpStatusCode.OK && content != null)
                {
                    JObject errorList = JObject.Parse(content);

                    foreach (var prop in errorList.Properties())
                    {
                        ret.Messages.Add($"\u2022 {prop.Value[0]}");

                        switch (prop.Name)
                        {
                            case "firstname": ret.FirstnameSet = false; break;
                            case "lastname": ret.LastnameSet = false; break;
                            case "email": ret.EmailSet = false; break;
                            case "message": ret.MessageSet = false; break;
                            default: break;
                        }
                    }

                    return ret;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return null;
        }

        public async Task<int> GetSpecificArticleId(int companyID, string key)
        {
            var (content, status) = await _restService.GetCall($"/api/v1/tenants/{companyID}/articles");
            if (status == HttpStatusCode.OK && content != null)
            {
                var jArray = JArray.Parse(content);
                List<ArticleSummary> summaries = JsonConvert.DeserializeObject<List<ArticleSummary>>(content, _jsonSerializerSettings);

                var summary = summaries.FirstOrDefault(x => x.headline.Equals(key));

                return summary.id;
            }
            return -1;
        }


        public async Task<string> GetSpecificArticle(int companyID, int articleId)
        {
            var (content, status) = await _restService.GetCall($"/api/v1/tenants/{companyID}/articles/{articleId}");
            if (status == HttpStatusCode.OK && content != null)
            {
                var jArray = JArray.Parse(content);
                var article = jArray.First.ToObject<JObject>().Value<string>("content");
                return article;
            }

            return null;
        }
    }
}
