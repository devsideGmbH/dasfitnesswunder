﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Dasfitnesswunder.Model;
using Dasfitnesswunder.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Dasfitnesswunder.Data
{
    public class CompanyDataService : ICompanyDataService
    {
        private readonly IRestService _restService;

        private JsonSerializerSettings _jsonSerializerSettings;

        public CompanyDataService(IRestService restService)
        {
            _restService = restService;
            _jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new SnakeCaseNamingStrategy()
                }
            };
        }

        public async Task<List<CompanySmall>> GetAll()
        {
            var (content, status) = await _restService.GetCall("/api/v1/tenants");

            if (status == HttpStatusCode.OK && content != null)
                return JsonConvert.DeserializeObject<List<CompanySmall>>(content, _jsonSerializerSettings);

            return null;
        }

        public async Task<Company> GetSpecific(int id)
        {
            var (content, status) = await _restService.GetCall($"/api/v1/tenants/{id}");

            if (status == HttpStatusCode.OK && content != null)
                return JsonConvert.DeserializeObject<List<Company>>(content, _jsonSerializerSettings)?[0];

            return null;
        }

        public async Task<List<RankingCategory>> GetAllRankings(int companyId)
        {
            var (content, status) = await _restService.GetCall($"/api/v1/tenants/{companyId}/rankings");

            if (status == HttpStatusCode.OK && content != null)
                return JsonConvert.DeserializeObject<List<RankingCategory>>(content, _jsonSerializerSettings);

            return null;
        }

        public async Task<List<UserRanking>> GetSpecificRanking(int companyId, int rankingId)
        {
            var (content, status) = await _restService.GetCall($"/api/v1/tenants/{companyId}/rankings/{rankingId}");

            if (status == HttpStatusCode.OK && content != null)
            {
                var ret = JsonConvert.DeserializeObject<List<UserRanking>>(content, _jsonSerializerSettings);
                return ret;
            }

            return null;
        }

        public async Task<List<RankingCategory>> GetDynamicRankings(int companyId)
        {
            var (content, status) = await _restService.GetCall($"/api/v1/tenants/{companyId}/dropdowns");

            if (status == HttpStatusCode.OK && content != null)
                return JsonConvert.DeserializeObject<List<RankingCategory>>(content, _jsonSerializerSettings);

            return null;
        }

        public async Task<List<UserRanking>> GetSpecificDynamicRanking(int companyId, int dropdownId)
        {
            var (content, status) = await _restService.GetCall($"/api/v1/tenants/{companyId}/dropdowns/{dropdownId}");

            if (status == HttpStatusCode.OK && content != null)
            {
                var ret = JsonConvert.DeserializeObject<List<UserRanking>>(content, _jsonSerializerSettings);
                return ret;
            }

            return null;
        }
    }
}