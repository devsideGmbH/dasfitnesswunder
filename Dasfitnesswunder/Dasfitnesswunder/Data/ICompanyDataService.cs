﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using Dasfitnesswunder.Model;

namespace Dasfitnesswunder.Data
{
    public interface ICompanyDataService
    {
        Task<List<CompanySmall>> GetAll();
        Task<Company> GetSpecific(int id);
        Task<List<RankingCategory>> GetAllRankings(int companyId);
        Task<List<UserRanking>> GetSpecificRanking(int companyId, int rankingId);
        Task<List<RankingCategory>> GetDynamicRankings(int companyId);    
        Task<List<UserRanking>> GetSpecificDynamicRanking(int companyId, int dropdownId);
    }
}