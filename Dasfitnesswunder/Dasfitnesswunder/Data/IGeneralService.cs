﻿using Dasfitnesswunder.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Dasfitnesswunder.Data
{
    public interface IGeneralService
    {
        Task<CreateUserResponse> CreateUser(int companyID, int? gender, int? dropdownValueID, /*DateTime dayOfBirth,*/ string nickName, string lastName, string firstName, string email, bool polarRegConfirm, bool agbConfirm, int? apiSourceID);

        Task<bool> SetThryveToken(int companyID, int userID, string thryveToken);

        Task<InfoRequestResponse> SendInfoRequest(string lastName, string firstName, string email, string message, string phone = "", string company = "");
        Task<int> GetSpecificArticleId(int companyID, string key);
        Task<string> GetSpecificArticle(int companyID, int articleId);

    }
}
