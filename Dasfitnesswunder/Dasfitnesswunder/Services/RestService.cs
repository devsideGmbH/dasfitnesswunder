﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Dasfitnesswunder.Services
{
    public class RestService : IRestService
    {
        private readonly HttpClient _httpClient;

        public RestService(string bearerToken)
        {
            _httpClient = new HttpClient(DependencyService.Get<IHttpClientHandlerService>().GetInsecureHandler())
            {
                BaseAddress = new Uri(Constants.BASEURL)
            };

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", bearerToken);
        }

        public async Task<(string Content, HttpStatusCode? Status)> GetCall(string collectionQuery)
        {
            try
            {
                var response = await _httpClient.GetAsync(collectionQuery);
                return response.IsSuccessStatusCode
                    ? (await response.Content.ReadAsStringAsync(), response.StatusCode)
                    : (null, response.StatusCode);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($@"\\tError {ex.Message}");
                return (null, null);
            }
        }

        public async Task<(string Content, HttpStatusCode? Status)> PostCall(string collectionQuery, string jsonBody)
        {
            try
            {
                var response = await _httpClient.PostAsync(collectionQuery,
                    new StringContent(jsonBody, Encoding.UTF8, "application/json"));

                return response.Content != null
                    ? (await response.Content.ReadAsStringAsync(), response.StatusCode)
                    : (null, response.StatusCode);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($@"\\tError {ex.Message}");
                return (null, null);
            }
        }

        public async Task<(string Content, HttpStatusCode? Status)> PutCall(string collectionQuery, string jsonBody)
        {
            try
            {
                var response = await _httpClient.PutAsync(collectionQuery,
                    new StringContent(jsonBody, Encoding.UTF8, "application/json"));

                return response.Content != null
                    ? (await response.Content.ReadAsStringAsync(), response.StatusCode)
                    : (null, response.StatusCode);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($@"\\tError {ex.Message}");
                return (null, null);
            }
        }

        public async Task<HttpStatusCode?> DeleteCall(string collectionQuery)
        {
            try
            {
                var response = await _httpClient.DeleteAsync(collectionQuery);
                return response.StatusCode;
            }
            catch (Exception ex)
            {
                Debug.WriteLine($@"\\tError {ex.Message}");
                return null;
            }
        }
    }
}