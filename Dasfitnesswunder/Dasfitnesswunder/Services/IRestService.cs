﻿using System.Net;
using System.Threading.Tasks;

namespace Dasfitnesswunder.Services
{
    public interface IRestService
    {
        Task<(string Content, HttpStatusCode? Status)> GetCall(string collectionQuery);
        Task<(string Content, HttpStatusCode? Status)> PostCall(string collectionQuery, string jsonBody);
        Task<(string Content, HttpStatusCode? Status)> PutCall(string collectionQuery, string jsonBody);
        Task<HttpStatusCode?> DeleteCall(string collectionQuery);
    }
}